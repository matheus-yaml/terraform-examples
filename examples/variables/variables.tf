variable "region" {
    default = "us-east-2"
}

variable "instance_type" {
    default = "t3.micro"
}

