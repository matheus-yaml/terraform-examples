module "network" {
    source  =   "./modules/network"

    aws_region  = var.aws_region
    name        = var.name
}

module "ec2" {
    source = "./modules/ec2"

    aws_region  = var.aws_region
    name        = var.name
    public_subnets = module.network.public_subnets
    private_subnets = module.network.private_subnets
}

