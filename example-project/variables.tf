variable "aws_region" {
    default = "us-east-2"
}

variable "name" {
    default = "example-project"
}

variable "public_az" {
    type = list
    default = ["a", "b"]
}

variable "private_az" {
    type = list
    default = ["a", "b"]
}