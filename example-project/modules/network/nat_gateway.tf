resource "aws_eip" "nat_gateway_eip" {
    vpc = true

}

resource "aws_nat_gateway" "private" {
    allocation_id   = aws_eip.nat_gateway_eip.id
    subnet_id       = aws_subnet.public_a.id

    tags = {
        Name = "${var.name}-private"
    }
}