resource "aws_internet_gateway" "public" {
    vpc_id = aws_vpc.example.id

    tags = {
        Name = "${var.name}-public"
    }
}