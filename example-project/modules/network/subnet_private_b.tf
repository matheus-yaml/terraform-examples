resource "aws_subnet" "private_b" {
    cidr_block = "10.0.4.0/24"
    vpc_id = aws_vpc.example.id
    availability_zone = "us-east-2b"

    tags = {
        Name = "${var.name}-private_b"
    }
}

resource "aws_route_table_association" "private_b" {
    route_table_id  = aws_route_table.private.id
    subnet_id       = aws_subnet.private_b.id
}