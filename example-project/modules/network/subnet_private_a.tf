resource "aws_subnet" "private_a" {
    cidr_block = "10.0.3.0/24"
    vpc_id = aws_vpc.example.id
    availability_zone = "us-east-2a"

    tags = {
        Name = "${var.name}-private_a"
    }

}

resource "aws_route_table_association" "private_a" {
    route_table_id  = aws_route_table.private.id
    subnet_id       = aws_subnet.private_b.id
}