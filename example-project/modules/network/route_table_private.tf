resource "aws_route_table" "private" {
    vpc_id = aws_vpc.example.id

    tags = {
        Name = "${var.name}-private"
    }
}

resource "aws_route" "private" {
    route_table_id          = aws_route_table.private.id
    destination_cidr_block  = "0.0.0.0/0"
    nat_gateway_id          = aws_nat_gateway.private.id
}