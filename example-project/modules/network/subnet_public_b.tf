resource "aws_subnet" "public_b" {
    cidr_block        = "10.0.2.0/24"
    vpc_id            = aws_vpc.example.id
    availability_zone = "us-east-2b"
    map_public_ip_on_launch = true

    tags = {
        Name = "${var.name}-public_b"
    }
}

resource "aws_route_table_association" "public_b" {
    route_table_id  = aws_route_table.public.id
    subnet_id       = aws_subnet.public_b.id
}