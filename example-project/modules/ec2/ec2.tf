resource "aws_instance" "public" {
    count = 2
    ami  = "ami-0a91cd140a1fc148a"
    instance_type    = "t3.micro"
    subnet_id = var.public_subnets[count.index]

    tags = {
      "Name" = "${var.name}-public ${count.index}"
    }
}